import firebase from "firebase";



const firebaseConfig = {
    apiKey: "AIzaSyBU7xPmID9186SlZaWCFKHZc-gHWdjzegg",
    authDomain: "blog-38845.firebaseapp.com",
    projectId: "blog-38845",
    storageBucket: "blog-38845.appspot.com",
    messagingSenderId: "739047771760",
    appId: "1:739047771760:web:901c9bdaf02498ef29eeb5",
    measurementId: "G-JS7ST1ZF6Q"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
firebase.analytics();

const storage = firebase.storage();

export {
    storage,
    firebase as default
}