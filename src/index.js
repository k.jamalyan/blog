import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './components/App/App';
import reportWebVitals from './reportWebVitals';
import {createStore,compose,applyMiddleware} from "redux";
import {Provider} from 'react-redux'
import {BrowserRouter} from "react-router-dom";
import rootReducrs from "./store/reducers/rootReducrs";
import thunk from "redux-thunk";

const composeEnhancers =
    typeof window === 'object' &&
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
        window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
        }) : compose;


const store = createStore(
    rootReducrs,
    composeEnhancers(
        applyMiddleware(thunk)
    )
)


ReactDOM.render(
    <Provider store={store}>
        <BrowserRouter>
            <App/>
        </BrowserRouter>
    </Provider>
    ,
    document.getElementById('root')
);


reportWebVitals();
